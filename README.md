#### Usage:
1.  Edit cron file as desired
2.  `docker-compose up -d`

Cron logs can be viewed by: `docker logs -f docker-cron`